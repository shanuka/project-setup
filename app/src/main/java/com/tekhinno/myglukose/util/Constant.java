/*
 * Copyright (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.tekhinno.myglukose.util;

/**
 * Created by shanuka on 8/10/16.
 */


public class Constant {

    /**
     * The constant EXTRA_EMAIL.
     */
    public static final String EXTRA_EMAIL = "";
    /**
     * The constant BASE_URL.
     */
    public static final String BASE_URL = "http://yaycollaborationapi.azurewebsites.net";
    /**
     * Instantiates a new Constant.
     */
    public Constant() {
    }
}
