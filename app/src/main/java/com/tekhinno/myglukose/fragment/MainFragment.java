package com.tekhinno.myglukose.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekhinno.myglukose.MainActivity;

import myglukose.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends BaseFragment {

    private static final String TAG = MainFragment.class.getCanonicalName();

    private FloatingActionButton floatingActionButtonAddContact;
    private RecyclerView recyclerViewContracts;
    private LinearLayoutManager mLayoutManager;

    /**
     * Instantiates a new Main fragment.
     */
    public MainFragment() {
    }

    /**
     * New instance main fragment.
     *
     * @return the main fragment
     */
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * On create view view.
     *
     * @param inflater           the inflater
     * @param container          the container
     * @param savedInstanceState the saved instance state
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        floatingActionButtonAddContact = (FloatingActionButton) view.findViewById(R.id.floatingActionButtonAddContact);

        floatingActionButtonAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        progressFragment = new ProgressFragment();
        recyclerViewContracts = (RecyclerView) view.findViewById(R.id.recyclerViewContracts);

        //use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerViewContracts.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());

        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        recyclerViewContracts.setLayoutManager(mLayoutManager);

        recyclerViewContracts.setItemAnimator(new DefaultItemAnimator());



        return view;
    }


    private void startActivity(Class target, Pair<View, String>[] pairs) {
        Intent i = new Intent(getActivity(), target);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairs);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

            startActivity(i, transitionActivityOptions.toBundle());

        } else {

            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().overridePendingTransition(
                    R.animator.push_right_in, R.animator.push_right_out);
        }
    }


}
