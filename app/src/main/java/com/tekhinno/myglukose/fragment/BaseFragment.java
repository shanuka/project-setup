/*
 * Copyright (c) 2016.
 */

package com.tekhinno.myglukose.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.octo.android.robospice.Jackson2SpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;


public class BaseFragment extends Fragment {

    public ProgressFragment progressFragment;
    protected SpiceManager spiceManager = new SpiceManager(
            Jackson2SpringAndroidSpiceService.class);

    public BaseFragment() {
        super();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onStart() {
        super.onStart();
//        if (!spiceManager.isStarted()) {
//            spiceManager.start(getActivity());
//        }

    }

    @Override
    public void onStop() {

//        if (spiceManager.isStarted()) {
//            spiceManager.shouldStop();
//        }
        super.onStop();

    }

    void removeProgressFragment() {
        if (progressFragment.isAdded()) {
            FragmentTransaction fragmenttransaction =
                    getActivity().getSupportFragmentManager().beginTransaction();

            fragmenttransaction.remove(progressFragment);

            fragmenttransaction.commit();
        }
    }

    void showProgressFragment(int id) {
        if (!progressFragment.isAdded()) {
            Log.e("isAdded", "isAdded");
            FragmentTransaction fragmenttransaction =
                    getActivity().getSupportFragmentManager().beginTransaction();

            fragmenttransaction.replace(id, progressFragment);
            // fragmenttransaction.addToBackStack(null);
            fragmenttransaction.commit();
        }
    }
}