/*
 * Copyright (c) 2016.
 */

package com.tekhinno.myglukose.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import myglukose.R;


/**
 * The type User profile fragment.
 */
public class UserProfileFragment extends Fragment {


    /**
     * Instantiates a new User profile fragment.
     */
    public UserProfileFragment() {
        // Required empty public constructor
    }

    /**
     * New instance user profile fragment.
     *
     * @return the user profile fragment
     */
    public static UserProfileFragment newInstance() {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }


    /**
     * On create view view.
     *
     * @param inflater           the inflater
     * @param container          the container
     * @param savedInstanceState the saved instance state
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        return view;
    }

    /**
     * On click.
     */
    public void onClick() {
        getActivity().setTitle(R.string.user_profile);
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

}
