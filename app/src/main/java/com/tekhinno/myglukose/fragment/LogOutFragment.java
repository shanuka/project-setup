/*
 * Copyright (c) 2016.
 */

package com.tekhinno.myglukose.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.tekhinno.myglukose.SplashScreensActivity;

/**
 * Created by shanuka on 4/28/16.
 */
public class LogOutFragment extends DialogFragment {


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        //  final AppPreferences mAppPreferences = new AppPreferences(getActivity());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Logout ");
        builder.setMessage("Are you want to logout?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        // mAppPreferences.putAccessToken(null);
                        dismiss();
                        startLoginActivity();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dismiss();

                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    private void startLoginActivity() {


        startActivity(new Intent(getActivity(), SplashScreensActivity.class));

        getActivity().finish();
    }
}
