/*
 * Copyright (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.tekhinno.myglukose;


import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import myglukose.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by shanuka on 8/16/16.
 */
public class ReferenceProjectApplication extends MultiDexApplication {


    @Override
    public void onCreate() {
        MultiDex.install(getApplicationContext());
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/gotham-rounded-book.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

    }
}
