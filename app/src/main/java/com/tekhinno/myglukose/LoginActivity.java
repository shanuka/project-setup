package com.tekhinno.myglukose;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.Visibility;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tekhinno.myglukose.fragment.ProgressFragment;
import com.tekhinno.myglukose.util.TransitionHelper;

import java.util.List;

import myglukose.R;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity {


    // UI references.
    @NotEmpty
    @Email
    private AutoCompleteTextView mEmailView;
    @NotEmpty
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputLayoutPassword;
    private Validator validator;


    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressFragment = new ProgressFragment();
        setupToolbar();


        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    clearErrors();
                    validator.validate();
                    return true;
                }
                return false;
            }
        });
        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                clearErrors();
                validator.validate();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        setupWindowAnimations();

        validator = new Validator(this);

        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                System.out.println("onValidationSucceeded");
                showProgressFragment(R.id.main_content);

            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(LoginActivity.this);

                    switch (view.getId()) {

                        case R.id.email:
                            textInputLayoutEmail.setError(message);
                            break;
                        case R.id.password:
                            textInputLayoutPassword.setError(message);
                            break;

                    }

                }
            }
        });
    }


    private void clearErrors() {
        textInputLayoutPassword.setError(null);
        textInputLayoutEmail.setError(null);
    }

    private void setupWindowAnimations() {
        Transition transition = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            transition = buildEnterTransition();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(transition);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private Visibility buildEnterTransition() {
        Slide enterTransition = new Slide();
        enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        enterTransition.setSlideEdge(Gravity.RIGHT);
        return enterTransition;
    }

    private void startMainActivity() {

        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(LoginActivity.this, true);
        startActivity(MainActivity.class, pairs);
    }

    private void startActivity(Class target, Pair<View, String>[] pairs) {
        Intent i = new Intent(LoginActivity.this, target);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LoginActivity.this, pairs);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

            startActivity(i, transitionActivityOptions.toBundle());
            finish();
        } else {

            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            overridePendingTransition(
                    R.animator.push_right_in, R.animator.push_right_out);
        }
    }


}

