/*
 * Copyright (c) 2016.
 */

package com.tekhinno.myglukose.models;


/**
 * The type User profile.
 */

public class UserProfile {

    /**
     * The Uid.
     */
    public String uid;
    private String username;
    private String email;
    private String phoneNumber;
    private String gender;
    private String materialstatus;
    private String haveChildren;

    /**
     * Instantiates a new User profile.
     */
    public UserProfile() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    /**
     * Instantiates a new User profile.
     *
     * @param username the username
     * @param email    the email
     */
    public UserProfile(String username, String email) {
        this.username = username;
        this.email = email;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets uid.
     *
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * Sets uid.
     *
     * @param uid the uid
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets phone number.
     *
     * @param phoneNumber the phone number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets gender.
     *
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets gender.
     *
     * @param gender the gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * Gets materialstatus.
     *
     * @return the materialstatus
     */
    public String getMaterialstatus() {
        return materialstatus;
    }

    /**
     * Sets materialstatus.
     *
     * @param materialstatus the materialstatus
     */
    public void setMaterialstatus(String materialstatus) {
        this.materialstatus = materialstatus;
    }

    /**
     * Gets have children.
     *
     * @return the have children
     */
    public String getHaveChildren() {
        return haveChildren;
    }

    /**
     * Sets have children.
     *
     * @param haveChildren the have children
     */
    public void setHaveChildren(String haveChildren) {
        this.haveChildren = haveChildren;
    }

}
// [END blog_user_class]
