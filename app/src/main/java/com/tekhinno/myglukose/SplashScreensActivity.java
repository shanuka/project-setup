package com.tekhinno.myglukose;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tekhinno.myglukose.util.AppPreferences;
import com.tekhinno.myglukose.util.TransitionHelper;
import com.tekhinno.myglukose.view.KenBurnsView;

import myglukose.R;


/**
 * The type Splash screens activity.
 */
public class SplashScreensActivity extends Activity {

    /**
     * The constant SPLASH_SCREEN_OPTION.
     */
    public static final String SPLASH_SCREEN_OPTION = "com.csform.android.uiapptemplate.SplashScreensActivity";
    /**
     * The constant SPLASH_SCREEN_OPTION_1.
     */
    public static final String SPLASH_SCREEN_OPTION_1 = "Option 1";
    /**
     * The constant SPLASH_SCREEN_OPTION_2.
     */
    public static final String SPLASH_SCREEN_OPTION_2 = "Option 2";
    /**
     * The constant SPLASH_SCREEN_OPTION_3.
     */
    public static final String SPLASH_SCREEN_OPTION_3 = "Option 3";

    private KenBurnsView mKenBurns;
    private ImageView mLogo;
    private TextView welcomeText;

    private Button buttonLogin;
    private AppPreferences mAppPreferences;
    private Button buttonSignUp;

    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar
        setContentView(R.layout.activity_splash_screen);

        mKenBurns = (KenBurnsView) findViewById(R.id.ken_burns_images);
        mLogo = (ImageView) findViewById(R.id.logo);
        welcomeText = (TextView) findViewById(R.id.welcome_text);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLoginActivity();
            }
        });
        buttonSignUp = (Button) findViewById(R.id.buttonSignUp);
        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSignUpActivity();
            }
        });
        mAppPreferences = new AppPreferences(getApplicationContext());
        String category = SPLASH_SCREEN_OPTION_3;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(SPLASH_SCREEN_OPTION)) {
            category = extras.getString(SPLASH_SCREEN_OPTION, SPLASH_SCREEN_OPTION_1);
        }
        setAnimation(category);
    }

    /**
     * Animation depends on category.
     */
    private void setAnimation(String category) {
        if (category.equals(SPLASH_SCREEN_OPTION_1)) {
            animation1();
        } else if (category.equals(SPLASH_SCREEN_OPTION_2)) {
            animation2();
        } else if (category.equals(SPLASH_SCREEN_OPTION_3)) {
            animation2();
            animation3();
        }
    }

    private void animation1() {
        ObjectAnimator scaleXAnimation = ObjectAnimator.ofFloat(mLogo, "scaleX", 5.0F, 1.0F);
        scaleXAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleXAnimation.setDuration(1200);
        ObjectAnimator scaleYAnimation = ObjectAnimator.ofFloat(mLogo, "scaleY", 5.0F, 1.0F);
        scaleYAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        scaleYAnimation.setDuration(1200);
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(mLogo, "alpha", 0.0F, 1.0F);
        alphaAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        alphaAnimation.setDuration(1200);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(scaleXAnimation).with(scaleYAnimation).with(alphaAnimation);
        animatorSet.setStartDelay(500);
        animatorSet.start();

        animatorSet.addListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                // TODO Auto-generated method stub

//                Log.d("mAppPreferences", "mAppPreferences.isEmptyAccessToken() " + mAppPreferences.isEmptyAccessToken());
//
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (mAppPreferences.isEmptyAccessToken()) {
//                            startMainActivity();
//                        } else {
//                            startLoginActivity();
//                        }
//                    }
//                }, 2000);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // TODO Auto-generated method stub

            }
        });

    }

    private void animation2() {
        mLogo.setAlpha(1.0F);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.translate_top_to_center);
        mLogo.startAnimation(anim);
    }

    private void animation3() {
        ObjectAnimator alphaAnimation = ObjectAnimator.ofFloat(welcomeText, "alpha", 0.0F, 1.0F);
        alphaAnimation.setStartDelay(1700);
        alphaAnimation.setDuration(500);
        alphaAnimation.start();

        if (true ) {
            alphaAnimation.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            startMainActivity();

                        }
                    }, 1000);
//                     User is logged in
//                    System.out.println("");
//                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//                    Log.e("user.getUid()", user.getUid());
//                    Log.e("user.getDisplayName()", user.getDisplayName());

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        } else {
            ObjectAnimator alphaAnimationButtonLogin = ObjectAnimator.ofFloat(buttonLogin, "alpha", 0.0F, 1.0F);
            alphaAnimationButtonLogin.setStartDelay(1700);
            alphaAnimationButtonLogin.setDuration(500);
            alphaAnimationButtonLogin.start();

            ObjectAnimator alphaAnimationButtonSignUp = ObjectAnimator.ofFloat(buttonSignUp, "alpha", 0.0F, 1.0F);
            alphaAnimationButtonSignUp.setStartDelay(1700);
            alphaAnimationButtonSignUp.setDuration(500);
            alphaAnimationButtonSignUp.start();
        }

    }

    private void startSignUpActivity() {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SplashScreensActivity.this, true);
        startActivity(SignUpActivity.class, pairs);
    }

    private void startLoginActivity() {

        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SplashScreensActivity.this, true);
        startActivity(LoginActivity.class, pairs);
    }

    private void startMainActivity() {

        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SplashScreensActivity.this, true);
        finish();
        startActivity(MainActivity.class, pairs);
    }

    private void startActivity(Class target, Pair<View, String>[] pairs) {
        Intent i = new Intent(SplashScreensActivity.this, target);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashScreensActivity.this, pairs);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

            startActivity(i, transitionActivityOptions.toBundle());

        } else {

            startActivity(new Intent(SplashScreensActivity.this, LoginActivity.class));
            overridePendingTransition(
                    R.animator.push_right_in, R.animator.push_right_out);
        }
    }
}
