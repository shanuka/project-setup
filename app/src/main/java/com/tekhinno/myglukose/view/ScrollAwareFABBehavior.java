/*
 * Copyright (c) 2016.
 */
package com.tekhinno.myglukose.view;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

/**
 * The type Scroll aware fab behavior.
 */
public class ScrollAwareFABBehavior extends FloatingActionButton.Behavior {
    /**
     * Instantiates a new Scroll aware fab behavior.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public ScrollAwareFABBehavior(Context context, AttributeSet attrs) {
        super();
    }

    /**
     * On start nested scroll boolean.
     *
     * @param coordinatorLayout the coordinator layout
     * @param child             the child
     * @param directTargetChild the direct target child
     * @param target            the target
     * @param nestedScrollAxes  the nested scroll axes
     * @return the boolean
     */
    @Override
    public boolean onStartNestedScroll(final CoordinatorLayout coordinatorLayout, final FloatingActionButton child,
                                       final View directTargetChild, final View target, final int nestedScrollAxes) {
        // Ensure we react to vertical scrolling
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL
                || super.onStartNestedScroll(coordinatorLayout, child, directTargetChild, target, nestedScrollAxes);
    }

    /**
     * On nested scroll.
     *
     * @param coordinatorLayout the coordinator layout
     * @param child             the child
     * @param target            the target
     * @param dxConsumed        the dx consumed
     * @param dyConsumed        the dy consumed
     * @param dxUnconsumed      the dx unconsumed
     * @param dyUnconsumed      the dy unconsumed
     */
    @Override
    public void onNestedScroll(final CoordinatorLayout coordinatorLayout, final FloatingActionButton child,
                               final View target, final int dxConsumed, final int dyConsumed,
                               final int dxUnconsumed, final int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
        if (dyConsumed > 0 && child.getVisibility() == View.VISIBLE) {
            // User scrolled down and the FAB is currently visible -> hide the FAB
            child.hide();
        } else if (dyConsumed < 0 && child.getVisibility() != View.VISIBLE) {
            // User scrolled up and the FAB is currently not visible -> show the FAB
            child.show();
        }
    }
}
