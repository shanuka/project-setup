package com.tekhinno.myglukose;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.Visibility;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.balysv.materialmenu.MaterialMenuDrawable;
import com.balysv.materialmenu.extras.toolbar.MaterialMenuIconCompat;
import com.tekhinno.myglukose.fragment.LogOutFragment;
import com.tekhinno.myglukose.fragment.MainFragment;
import com.tekhinno.myglukose.fragment.UserProfileFragment;

import java.util.List;

import myglukose.R;

/**
 * The type Main activity.
 */
public class MainActivity extends AppCompatActivity {

    private MaterialMenuIconCompat materialMenu;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private boolean isDrawerOpened = false;
    private Toolbar toolbar;
    private UserProfileFragment mUserProfileFragment;
    private MainFragment mMainFragment;

    /**
     * On create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        materialMenu = new MaterialMenuIconCompat(this, Color.WHITE, MaterialMenuDrawable.Stroke.THIN); // or retrieve from your custom view, etc
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        setupDrawerContent(mNavigationView);
        mDrawerLayout.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                materialMenu.setTransformationOffset(
                        MaterialMenuDrawable.AnimationState.BURGER_ARROW,
                        isDrawerOpened ? 2 - slideOffset : slideOffset
                );
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                isDrawerOpened = true;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                isDrawerOpened = false;
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                if (newState == DrawerLayout.STATE_IDLE) {
                    if (isDrawerOpened) materialMenu.setState(MaterialMenuDrawable.IconState.ARROW);
                    else materialMenu.setState(MaterialMenuDrawable.IconState.BURGER);
                }
            }
        });

        mUserProfileFragment = UserProfileFragment.newInstance();
        mMainFragment = mMainFragment.newInstance();

        setMainFragment();
        setupWindowAnimations();
    }

    /**
     * On create options menu boolean.
     *
     * @param menu the menu
     * @return the boolean
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * On options item selected boolean.
     *
     * @param item the item
     * @return the boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!isDrawerOpened) {
                mDrawerLayout.openDrawer(mNavigationView);
            }
            System.out.println("home");
            return false;
        }


        return super.onOptionsItemSelected(item);
    }


    /**
     * On back pressed.
     */
    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        List<Fragment> frags = getSupportFragmentManager().getFragments();
        Fragment lastFrag = getLastNotNull(frags);
        //nothing else in back stack || nothing in back stack is instance of our interface

        Log.e("lastFrag", "lastFrag" + lastFrag.toString());
//        if (count == 0 || !(lastFrag instanceof TimeLineSearchragment)) {
//          //  super.onBackPressed();
//        } else {
//            ((TimeLineSearchragment)lastFrag).onClick();
//        }
        if (lastFrag instanceof UserProfileFragment) {
            ((UserProfileFragment) lastFrag).onClick();
            setMainFragment();
        }


        if (lastFrag instanceof MainFragment) {
            super.onBackPressed();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAfterTransition();
            }
        }
    }

    private Fragment getLastNotNull(List<Fragment> list) {
        for (int i = list.size() - 1; i >= 0; i--) {
            Fragment frag = list.get(i);
            if (frag != null) {
                return frag;
            }
        }
        return null;
    }

    /**
     * Dp to px int.
     *
     * @param dp the dp
     * @return the int
     */
    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    /**
     * On post create.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        isDrawerOpened = mDrawerLayout.isDrawerOpen(GravityCompat.START); // or END, LEFT, RIGHT
        materialMenu.syncState(savedInstanceState);
    }

    /**
     * On save instance state.
     *
     * @param outState the out state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        materialMenu.onSaveInstanceState(outState);
    }

    private void setupDrawerContent(NavigationView mNavigationView) {

        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {
                            case R.id.user_profile:
                                // TimeLine2Activity.launch(TagCloudActivity.this);
                                setUserProfileFragment();
                                toolbar.setTitle(R.string.user_profile);
                                break;
                            case R.id.log_out:
                                LogOutFragment mFireMissilesDialogFragment = new LogOutFragment();
                                mFireMissilesDialogFragment.show(getSupportFragmentManager(), "LogOutFragment");
                                break;
                            default:

                                break;
                        }

                        return true;
                    }
                });
    }

    private void setUserProfileFragment() {
        if (!mUserProfileFragment.isAdded()) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.main_content, mUserProfileFragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
            getFragmentManager().executePendingTransactions();
        }
    }

    private void setMainFragment() {
        if (!mMainFragment.isAdded()) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.main_content, mMainFragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
            getFragmentManager().executePendingTransactions();
        }
    }

    private void setupWindowAnimations() {
        Transition transition = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            transition = buildEnterTransition();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(transition);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private Visibility buildEnterTransition() {
        Explode enterTransition = new Explode();
        enterTransition.setDuration(getResources().getInteger(R.integer.anim_duration_long));
        return enterTransition;
    }

}
