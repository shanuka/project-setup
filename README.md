# Best practices in Android development
Following these guidelines will use for Android application devlopment in Exilesoft.

### Android SDK

Place your Android SDK somewhere in your home directory or some other application-independent location. Some distributions of IDEs include the SDK when installed, and may place it under the same directory as the IDE. This can be bad when you need to upgrade (or reinstall) the IDE, as you may lose your SDK installation, forcing a long and tedious redownload.

Example :
~~~
Windows
c:/Users/<user name>/android_sdk
Mac
/Users/<user>android_sdk
~~~
### Development IDE

The recommended IDE is [Android Studio](https://developer.android.com/sdk/installing/studio.html) because it is developed and frequently updated by Google, has good support for Gradle, contains a range of useful monitoring and analysis tools and is generally tailored for Android development.

### Build system

Your default option should be [Gradle](http://tools.android.com/tech-docs/new-build-system). With Gradle, it's simple to:

* Build different flavours or variants of your app
* Make simple script-like tasks
* Manage and download dependencies
* Customize keystores
* And more

Ant, the previously supported build system has been deprecated since 2015 and now only Android's Gradle plugin is being actively developed by Google.

It is important that your application's build process is defined by your Gradle files, rather than being reliant on IDE specific configurations. This allows for consistent builds between tools and better support for continuous integration systems. 

### Project structure

Although Gradle offers a large degree of flexibility in your project structure, unless you have a compelling reason to do otherwise, you should accept its [default structure](http://tools.android.com/tech-docs/new-build-system/user-guide#TOC-Project-Structure). This will simplify your build scripts.

### Gradle configuration

**General structure.** Follow [Google's guide on Gradle for Android](http://tools.android.com/tech-docs/new-build-system/user-guide)

**Passwords.** In your app's `build.gradle` you will need to define the `signingConfigs` for the release build. Here is what you should avoid:

_Don't do this_. This would appear in the version control system.

```android
signingConfigs {
    release {
        storeFile file("myapp.keystore")
        storePassword "password123"
        keyAlias "thekey"
        keyPassword "password789"
    }
}
```
Instead, make a `gradle.properties` file which should _not_ be added to the version control system:

```
KEYSTORE_PASSWORD=password123
KEY_PASSWORD=password789
```

That file is automatically imported by Gradle, so you can use it in `build.gradle` as such:

```android
signingConfigs {
    release {
        try {
            storeFile file("myapp.keystore")
            storePassword KEYSTORE_PASSWORD
            keyAlias "thekey"
            keyPassword KEY_PASSWORD
        }
        catch (ex) {
            throw new InvalidUserDataException("You should define KEYSTORE_PASSWORD and KEY_PASSWORD in gradle.properties.")
        }
    }
}
```


**Prefer Maven dependency resolution instead of importing jar files.** If you explicitly include jar files in your project, they will be of some specific frozen version, such as `2.1.1`. Downloading jars and handling updates is cumbersome, this is a problem that Maven solves properly, and is also encouraged in Android Gradle builds. For example:

```android
dependencies {
    compile 'com.squareup.okhttp:okhttp:2.2.0'
    compile 'com.squareup.okhttp:okhttp-urlconnection:2.2.0'
}
```    

**Avoid Maven dynamic dependency resolution**
Avoid the use of dynamic dependency versions, such as `2.1.+` as this may result in different and unstable builds or subtle, untracked differences in behavior between builds. The use of static versions such as `2.1.1` helps create a more stable, predictable and repeatable development environment.

**Use different package name for non-release builds**
Use `applicationIdSuffix` for *debug* [build type](http://tools.android.com/tech-docs/new-build-system/user-guide#TOC-Build-Types) to be able to install both *debug* and *release* apk on the same device (do this also for custom build types, if you need any). This will be especially valuable later on in the app's lifecycle, after it has been published to the store.

```android
android {
    buildTypes {
        debug {
            applicationIdSuffix '.debug'
            versionNameSuffix '-DEBUG'
        }

        release {
            // ...
        }
    }
}
```

Use different icons to distinguish the builds installed on a device—for example with different colors or an overlaid  "debug" label. Gradle makes this very easy: with default project structure, simply put *debug* icon in `app/src/debug/res` and *release* icon in `app/src/release/res`. You could also [change app name](http://stackoverflow.com/questions/24785270/how-to-change-app-name-per-gradle-build-type) per build type, as well as  `versionName` (as in the above example)

### Libraries

**[Jackson 2 ](http://wiki.fasterxml.com/JacksonHome)** is a Java library for converting Objects into JSON and vice-versa. [Gson](https://code.google.com/p/google-gson/) is a popular choice for solving this problem, however we find Jackson to be more performant since it supports alternative ways of processing JSON: streaming, in-memory tree model, and traditional JSON-POJO data binding. Keep in mind, though, that Jackson is a larger library than Gson, so depending on your case, you might prefer Gson to avoid 65k methods limitation. 

Gradle  Dependence link for latest [version](https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind)  

~~~
compile 'com.fasterxml.jackson.core:jackson-databind:2.8.1'
compile 'com.fasterxml.jackson.core:jackson-core:2.8.1'
compile 'com.fasterxml.jackson.core:jackson-annotations:2.8.1'
~~~

<a name="networklibs"></a>
**Networking, caching, and images.** There are a couple of battle-proven solutions for performing requests to backend servers, which you should use perform considering implementing your own client. 
Use [RestTemplate](http://docs.spring.io/spring-android/docs/1.0.1.RELEASE/reference/html/) performing requests . This includes the ability to use RestTemplate as the REST client for your Android applications. Spring for Android also provides support for integrating Spring Social functionality into your Android application, which includes a robust OAuth based, authorization client and implementations for popular social web sites, such as Twitter and Facebook. and its support JSON, XML,

Features

*  A Rest Client for Android
*  Auth support for accessing secure APIs

[RoboSpice](https://github.com/stephanenicolas/robospice) is a modular android library that makes writing asynchronous long running tasks easy. It is specialized in network requests, supports caching and offers REST requests out-of-the box using extension modules.
features

* executes network requests asynchronously (in a background Android Service)
* supports REST out of the box (using Spring Android or Google Http Client or Retrofit).
* is strongly typed ! You query web services using POJOs as parameters and you get POJOs as request results.
* enforces no constraints neither on POJOs used for requests nor on Activity classes you use in your projects
* **caches results in Json with both Jackson or Jackson2 or Gson, or Xml, or flat text files, or binary files, even using ORM Lite (still in beta)**
* notifies your activities (or any other context) of the result of the network request with respect to their life cycles.
* notifies your activities (or any other context) on the UI Thread
* no memory leaks at all, like Android Loaders, unlike Android AsyncTasks
* uses a simple but robust exception handling model
* supports multi-threading of request executions
* is stable, efficient and designed to respect Android philosophy
* supports request cancelling, request prioritization and requests aggregation
* supports aggregation of different web services

Use spring rest template with robospice. 

Dependency 
~~~
compile 'com.octo.android.robospice:robospice:1.4.14'
compile 'com.octo.android.robospice:robospice-spring-android:1.4.14'
~~~

**RxJava** is a library for Reactive Programming, in other words, handling asynchronous events. It is a powerful and promising paradigm, which can also be confusing since it's so different. We recommend to take some caution before using this library to architect the entire application. There are some projects done by us using RxJava.


If you have no previous experience with Rx, start by applying it only for responses from the API. Alternatively, start by applying it for simple UI event handling, like click events or typing events on a search field. If you are confident in your Rx skills and want to apply it to the whole architecture, then write Javadocs on all the tricky parts. Keep in mind that another programmer unfamiliar to RxJava might have a very hard time maintaining the project. Do your best to help them understand your code and also Rx.

**[Retrolambda](https://github.com/evant/gradle-retrolambda)** is a Java library for using Lambda expression syntax in Android and other pre-JDK8 platforms. It helps keep your code tight and readable especially if you use a functional style with for example with RxJava.

Android Studio offers code assist support for Java8 lambdas. If you are new to lambdas, just use the following to get started:

- Any interface with just one method is "lambda friendly" and can be folded into the more tight syntax
- If in doubt about parameters and such, write a normal anon inner class and then let Android Studio fold it into a lambda for you.

**Custom fonts in Android [Calligraphy](https://github.com/chrisjenx/Calligraphy)**

Calligraphy used for custom fonts for android application. this is the easy way to change font in android application ( no need to create custom views )

Dependency 
~~~
compile 'uk.co.chrisjenx:calligraphy:XXX'
~~~

**Android Validation Framework**

Android [Saripaar](https://github.com/ragunathjawahar/android-saripaar) is a simple, feature-rich and powerful rule-based UI form validation library for Android. It is the SIMPLEST UI validation library available for Android.

* Built on top of Apache Commons Validator, a validation framework with proven track record on the web, desktop and mobile platforms.
* Declarative style validation using Annotations.
* Extensible, now allows Custom Annotations.
* Synchronous and Asynchronous validations, you don't have to worry about threading.


### 65k methods limitation

Avoiding the 64K Limit Configuring Your App for [Multidex](https://developer.android.com/studio/build/multidex.html) with Gradle please use multidex configuration on gradle file.

Dependency  

~~~
compile 'com.android.support:multidex:1.0.1'
~~~

### Improve quality and syntax o code

Improving Android code with automatic tools such as [Checkstyle](http://checkstyle.sourceforge.net/), [Findbugs](http://findbugs.sourceforge.net/),  [PMD](https://pmd.github.io/), and of course [Android Lint](http://tools.android.com/tips/lint). 

##### Checkstyle

“Checkstyle is a development tool to help programmers write Java code that adheres to a coding standard. It automates the process of checking Java code to spare humans of this boring (but important) task.”

As said by the  developers of Checkstyle, this tool help you to define and maintain in a very precise and flexible way the coding standards of a project. When you launch Checkstyle, it will parse your Java code and will inform you about all the errors found according the configuration provided.

##### Findbugs

“FindBugs uses static analysis to inspect Java bytecode for occurrences of bug patterns.” Findbugs basically just need the bytecode of a program to do the analysis, so it is very easy to use. It will detect common error such as wrong boolean operator. Findbugs is also able to detect error due to misunderstood of language features, such as reassignment of parameters in Java (which is not really possible since it parameters are passed by values).

##### PMD

In the facts, PMD is a very powerful tool which works a little bit like Findbugs, but inspect directly the source code, and not the bytecode (btw. PMD can work with plenty of languages). The goal is globally the same, find patterns which can lead to bugs using static analysis. So why use Findbugs and PMD at the same time ? Well, even if Findbugs and PMD share globally the same goals, their inspecting methods are different. So PMD can sometimes find bugs which Findbugs wont, and vice versa.

##### Android Lint

“The Android lint tool is a static code analysis tool that checks your Android project source files for potential bugs and optimization improvements for correctness, security, performance, usability, accessibility, and internationalization.”

As the official site said, Android Lint is another static analysis tool, dedicated this time to Android. It is extremely powerful, and can give great piece of advice to improve the quality of your code.

![Screen Shot 2016-08-10 at 1.24.07 PM.jpg](https://bitbucket.org/repo/9B4j7X/images/2788228301-Screen%20Shot%202016-08-10%20at%201.24.07%20PM.jpg)

### Java packages architecture

Java architectures for Android applications can be roughly approximated in [Model-View-Controller](http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller). In Android, [Fragment and Activity are actually controller classes](http://www.informit.com/articles/article.aspx?p=2126865). On the other hand, they are explicitly part of the user interface, hence are also views.

For this reason, it is hard to classify fragments (or activities) as strictly controllers or views. It's better to let them stay in their own `fragments` package. Activities can stay on the top-level package as long as you follow the advice of the previous section. If you are planning to have more than 2 or 3 activities, then make also an `activities` package.

Otherwise, the architecture can look like a typical MVC, with a `models` package containing POJOs to be populated through the JSON parser with API responses, and a `views` package containing your custom Views, notifications, action bar views, widgets, etc. Adapters are a gray matter, living between data and views. However, they typically need to export some View via `getView()`, so you can include the `adapters` subpackage inside `views`.

Some controller classes are application-wide and close to the Android system. These can live in a `managers` package. Miscellaneous data processing classes, such as "DateUtils", stay in the `utils` package. Classes that are responsible for interacting with the backend stay in the `network` package.

All in all, ordered from the closest-to-backend to the closest-to-the-user:

```
com.exilesoft.project
├─ network
├─ models
├─ managers
├─ utils
├─ fragments
└─ views
   ├─ adapters
   ├─ actionbar
   ├─ widgets
   └─ notifications
```